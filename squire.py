import os

import discord
from dotenv import load_dotenv
from models import db, ListItem

import responses

#method to manually recreate the squire database if needed
#to run run python3 in the command line and enter the following commands:
#   from squire import createTables
#   createTables()
def createTables():
    from peewee import SqliteDatabase

    db = SqliteDatabase('squire.db')
    db.connect()
    db.create_tables([ListItem])


def setUpDbIfNeeded():
    if ListItem.getListByName('Master') is None:
        newItem = ListItem()
        newItem.name = 'Master'
        newItem.save()
        print('Created a new Master List.')

async def send_message(message, user_message, is_private):
    try:
        response = responses.handle_response(user_message)
        await message.author.send(response) if is_private else await message.channel.send(response)
    except Exception as e:
        print(e)

def run_discord_bot():
    load_dotenv()
    db.connect()
    setUpDbIfNeeded()
    TOKEN = os.environ['DISCORD_TOKEN']

    intents = discord.Intents.default()
    intents.message_content = True
    client = discord.Client(intents = intents)

    @client.event
    async def on_ready():
        print(f'{client.user} is now running!')

    @client.event
    async def on_message(message):
        if message.author == client.user:
            return

        username = str(message.author)
        user_message = str(message.content)
        channel = str(message.channel)

        print(f"{username} said: '{user_message}' ({channel})")

        if user_message[0] == '?':
            user_message = user_message[1:]
            await send_message(message, user_message, is_private=True)
        else:
            await send_message(message, user_message, is_private=False)

    client.run(TOKEN)
