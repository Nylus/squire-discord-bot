import re

from models import ListItem

def handle_response(message) -> str:
    p_message = message
    #p_message = message.lower() <--- replace this, currently it causes issues with the database 9.e. a capitolized list name cannot be found
    #because it converts it to a lowercase which will not exist in the database

    if p_message == 'hello':
        return 'Hey there!'

    if p_message == 'menu':
        return f'''{ListItem.getListsSummary()}

If you need anything else, please don't hesitate to ask for '!help'. '''

    if p_message == '!help':
        return '''`!create <list name>` to create a new list.
`!display <list name>` to show the contents of the list.
`!add` or `!+ <list item> #<list name>` to add an item to a specified list, one at a time.
`!check` or `!x #<item number> #<list name>` to strike an item from a specified list, marking it as complete.
`!remove` or `!- #<item number> #<list name>` to remove an item from a specified list, one at a time.
`!delete` or `!del #<list name>` to delete an entire list.

`!secret` for me to share something special with you.'''

    if p_message.startswith('!add') or p_message.startswith('!+'):
        #store the user's list item to the component variable by parsing user's text and capturing all characters detected between the !add command
        # or the !+ command and # character before the list name.
        components = re.search(r'^(?:!add|!\+) ([^#]+)#(.*)$', p_message)
        #note: add error handling here later.
        listItemDescription = components.group(1)
        listName = components.group(2)
        parentItem = ListItem.getListByName(listName)
        if parentItem != None:
            newItem = ListItem()
            newItem.name = listItemDescription
            newItem.parent = parentItem
            newItem.save()
            print(f'created list item {listItemDescription} in list {listName}')
