from peewee import *

db = SqliteDatabase('squire.db')

class ListItem(Model):
    #variable for list title name.
    name = CharField()
    #variable for name of list item's ID relative to other item's of its parent's list
    listIndex = IntegerField(null=True)
    #variable for holding checked status.
    #every list and item will have a default state of not require a check status.
    checked = BooleanField(null=True)
    #variable for linking an item within a list to a parent.
    #every list item will have a parent associated with them.
    parent = ForeignKeyField('self', null=True, backref = 'items')

    @staticmethod
    def getListsSummary():
        #variable that is definted by chain methods to find and display all lists within the Master List.
        masterList = ListItem.getListByName('Master')
        if len(masterList.items) == 0:
            return '''I'm sorry, I am not currently helping you keep track of anything.'''
        else:
            output = '''Here are the current things I am helping keep track of for you:\n'''
            for item in masterList.items:
                output += f'\n* {item.name}'
            return output

    @staticmethod
    def getListByName(listName):
        try:
            results = ListItem.select().where(ListItem.name == listName).get()
        except ListItem.DoesNotExist:
            results = None
        return results
    #TODO: define a way to automatical assign the list index from 1
    #def setParent(parentItem) temporarily commented out, need to complete

    class Meta:
        database = db # This model uses the "people.db" database.
